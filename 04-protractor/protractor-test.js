'use strict';

describe('04-protractor example', function() {

  browser.get('index.html');

  it('should have initial settings', function() {
    var userNameInputElement = element(by.model('vm.user.name'));
    var userNameInput = userNameInputElement.getAttribute('value');
    expect(userNameInput).toBe('Petr');

    var userNameTextElement = element(by.binding('vm.user.name'));
    var userNameText = userNameTextElement.getText();
    expect(userNameText).toMatch('Petr');

    expect(element(by.model('vm.user.surname')).getAttribute('value')).toBe('Novák');
    expect(element(by.binding('vm.user.surname')).getText()).toBe('Příjmení: Novák');
  });

  it('should change name if model is changed', function() {
    var userNameElement = element(by.model('vm.user.name'));
    userNameElement.clear().sendKeys('Josef');

    var userNameText = element(by.binding('vm.user.name'));
    expect(userNameText.getText()).toMatch('Josef');
  });

  // TODO 4 - vytvořte test pojmenovaný - should change age if yearOfBirth is changed

});
