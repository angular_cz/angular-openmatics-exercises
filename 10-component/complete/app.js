'use strict';

var tabComponent = {
  require: {
    tabsetController: '^tabset'
  },
  templateUrl: 'tab.html',
  bindings: {
    header: '@'
  },
  transclude: true,
  controller: function() {
    this.$onInit = function() {
      this.tabsetController.add(this);
    }
  },
  controllerAs: 'tab'
};

var tabsetComponent = {
  templateUrl: 'tabset.html',
  transclude: true,

  controller: TabsetController,
  controllerAs: 'tabset'
};

function TabsetController($log) {
  this.tabs = [];

  var hide = function(tab) {
    tab.active = false;
  };

  this.add = function(tab) {
    $log.info('Tab has been added:', tab.header);

    this.tabs.push(tab);
    hide(tab);

    if (this.tabs.length == 1) {
      this.select(tab);
    }
  };

  this.select = function(tab) {
    this.tabs.forEach(hide);

    tab.active = true;
  };
}

angular.module('componentApp', [])
  .component('tab', tabComponent)
  .component('tabset', tabsetComponent);
