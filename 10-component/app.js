'use strict';

// TODO 3.1 - přidejte závislost na komponentě tabset
var tabComponent = {
  templateUrl: 'tab.html',
  transclude: true,

  bindings: {
    header: '@'
  },

  controllerAs: 'tab',
  controller: function() {
  }
};

// TODO 2.3 - přidejte komponentě controller
var tabsetComponent = {
  templateUrl: 'tabset.html',
  transclude: true
};

function TabsetController($log) {
  this.tabs = [];

  var hide = function(tab) {
    tab.active = false;
  };

  this.add = function(tab) {
    $log.info('Tab has been added:', tab.header);

    this.tabs.push(tab);
    hide(tab);

    if (this.tabs.length == 1) {
      this.select(tab);
    }
  };

  this.select = function(tab) {
    this.tabs.forEach(hide);

    tab.active = true;
  };
}


// TODO 2.1 - přidejte komponentu tabset
angular.module('directiveApp', [])

  .component('tab', tabComponent);
