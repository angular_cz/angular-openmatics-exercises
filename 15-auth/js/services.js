'use strict';

function orderFactory(REST_URI, $resource) {
  return $resource(REST_URI + '/orders/:id', {"id": "@id"});
}

function AuthService($http, REST_URI, $rootScope, authStorage, $q) {

  var LOGIN_URI = REST_URI + "/login";

  this.login = function(name, pass) {
    var credentials = {name: name, password: pass};

    // TODO 1.1 - odešlete přihlašovací údaje

  };

  this.successLogin = function(response) {
    var user = response.data;
    authStorage.setUser(user);

    // TODO 1.2 - přidejte token do defaultní hlavičky
    var AUTH_HEADER = 'X-Auth-Token';


    $rootScope.$broadcast("login:loginSuccess");
    return response;
  };

  this.failedLogin = function(response) {
    $rootScope.$broadcast("login:loginFailed");
    return $q.reject(response);
  };

  this.logout = function() {
    authStorage.setUser(null);
    $rootScope.$broadcast("login:loggedOut");
  };

  this.isAuthenticated = function() {
    return Boolean(authStorage.getUser());
  };

  this.getUserName = function() {
    return authStorage.getUser() && authStorage.getUser().name;
  };

  this.getToken = function() {
    return this.isAuthenticated() && authStorage.getUser().token;
  };

  this.hasRole = function(role) {
    return this.isAuthenticated() && authStorage.getUser().roles.indexOf(role) !== -1;
  };
}

function AuthStorage($sessionStorage) {
  this.authStorage = $sessionStorage.$default({
    user: null
  });

  this.getUser = function() {
    return this.authStorage.user;
  };

  this.setUser = function(user) {
    this.authStorage.user = user;
  };
}

angular.module('authApp')
  .factory('Order', orderFactory)
  .service('authStorage', AuthStorage)
  .service('authService', AuthService);
