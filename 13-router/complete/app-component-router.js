'use strict';

function OrderListController($http, REST_URI) {
  this.orders = [];

  this.statuses = {
    NEW: 'Nová',
    MADE: 'Vyrobená',
    CANCELLED: 'Zrušená',
    PAID: 'Zaplacená',
    SENT: 'Odeslaná'
  };

  this.onOrdersLoad = function(orders) {
    this.orders = orders;
  };

  $http.get(REST_URI + '/orders')
    .then(function(response) {
      return response.data;
    })
    .then(this.onOrdersLoad.bind(this));
}

function OrderCreateController($http, $location, REST_URI) {
  this.save = function(order) {
    $http.post(REST_URI + '/orders', order)
      .then(function() {
        $location.path('/orders');
      });
  }
}

angular.module('orderAdministration', ['ngComponentRouter', 'ngMessages'])
  .constant('REST_URI', '//angular-cz-orders-api.herokuapp.com')
  .component('orderList', {
    templateUrl: 'orderList.html',
    controller: OrderListController,
    controllerAs: 'list'
  })

  .value('$routerRootComponent', 'app')

  .component('app', {
    $routeConfig: [
      {path: '/orders', name: 'OrderList', component: 'orderList', useAsDefault: true},
      {path: '/detail/:id', name: 'OrderDetail', component: 'orderDetail'},
      {path: '/create', name: 'OrderCreate', component: 'orderCreate'}
    ],
    template: '<ng-outlet></ng-outlet>'
  })

  .component('orderDetail', {
    templateUrl: 'orderDetail.html',
    controllerAs: 'detail',
    controller: function($http, REST_URI) {
      this.$routerOnActivate = function(next) {
        var id = next.params.id;

        return $http.get(REST_URI + '/orders/' + id)
          .then(function(response) {
            this.order = response.data;
          }.bind(this));
      }
    }
  })

  .component('orderCreate', {
    templateUrl: 'orderCreate.html',
    controller: OrderCreateController,
    controllerAs: 'create'
  });
