'use strict';

var fakeData = ['first fake paragraph', 'second fake paragraph'];

// TODO - postupně budujte obsah definičního objektu komponenty
var baconIpsumComponent = {};

angular.module('bacon', ['ipsumService'])
  .controller('BaconController', BaconController)
  .component('baconIpsum', baconIpsumComponent);

function BaconController() {
  this.title = 'Bacon-ipsum title';
  this.paragraphs = 3;

  this.onChangeCallback = function(data) {
    console.log('Loaded paragraphs', data);
  }
}
