'use strict';

function orderFactory(REST_URI, $resource) {
  return $resource(REST_URI + '/auth-02/orders/:id', {"id": "@id"});
}

function AuthService($http, REST_URI, $rootScope, $q) {

  var LOGIN_URI = REST_URI + "/login";

  var user = null;

  this.login = function(name, pass) {
    return $http.post(LOGIN_URI, {name: name, password: pass})
      .then(this.successLogin.bind(this))
      .catch(this.failedLogin.bind(this));
  };

  this.successLogin = function(response) {
    user = response.data;
    $rootScope.$broadcast("login:loginSuccess");
    return response;
  };

  this.failedLogin = function(response) {
    $rootScope.$broadcast("login:loginFailed");
    return $q.reject(response);
  };

  this.logout = function() {
    user = null;
    $rootScope.$broadcast("login:loggedOut");
  };

  this.isLoginUrl = function(url) {
    return LOGIN_URI === url;
  };

  this.isAuthenticated = function() {
    return Boolean(user);
  };

  this.getUserName = function() {
    return this.isAuthenticated() && user.name;
  };

  this.getToken = function() {
    return this.isAuthenticated() && user.token;
  };
}

function AuthModal($modal) {

  this.modalInstance = null;

  this.showLoginModal = function() {
    if (!this.modalInstance) {

      this.modalInstance = $modal.open({
        templateUrl: 'authModal.html',
        controller: 'AuthCtrl',
        controllerAs: 'auth'
      });

      this.modalInstance.result.finally(function() {
        delete this.modalInstance;
      }.bind(this));
    }

    return this.modalInstance.result;
  };
}

angular.module('authApp')
  .factory('Order', orderFactory)
  .service('authService', AuthService)
  .service('authModal', AuthModal);
