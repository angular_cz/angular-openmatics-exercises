'use strict';

function CalculatorProvider() {
  var pagePrice = 3;

  this.setPagePrice = function(price) {
    pagePrice = price;
  };

  this.$get = function($log) {
    return {
      getPrice: function(product) {

        var price = 0;
        var baseCoverPrice = 70;
        switch (product.pageSize) {
          case 'A6':
            price += baseCoverPrice;
            break;
          case 'A5':
            price += baseCoverPrice + 20;
            break;
          case 'A4':
            price += baseCoverPrice + 40;
            break;
        }

        var pagesPrice = Math.ceil(product.numberOfPages / 5) * pagePrice;
        price += pagesPrice;

        $log.log('Price : ' + price + " - " + product.numberOfPages + " stran, " + product.pageSize);
        return price;
      }
    };
  };
}

angular.module('diServices', [])
  .provider('calculator', CalculatorProvider);


// -----------------


function CalculatorController(defaultProduct, calculator) {
  this.product = defaultProduct;

  this.getPrice = function() {
    return calculator.getPrice(this.product);
  };
}

var defaultProduct = {
  pageSize: 'A5',
  numberOfPages: 50
};

angular.module('diApp', ['diServices'])
  .constant('PAGE_PRICE', 4)

  .config(function(calculatorProvider, PAGE_PRICE) {
    calculatorProvider.setPagePrice(PAGE_PRICE);
  })

  .value('defaultProduct', defaultProduct)

  .controller('CalculatorCtrl', CalculatorController);
